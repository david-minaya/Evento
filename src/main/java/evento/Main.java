package evento;

/**
 * Creada por david minaya el 31/10/2017
 *
 * En esta clase se muestra como implementar un evento simple.
 */
public class Main {

    public static void main(String[] args) {

        Evento evento = new Evento();

        evento.addEventoListener(new EventoListener() {
            @Override
            public void evento(Evento evento) {
                System.out.println("Ejecutando evento");
            }
        });

        evento.lanzar();
    }
}
