package multiples_listeners;

/**
 * Creada por david minaya el 31/10/2017
 *
 * En esta clase se muestra como tener varios
 * listener de un solo evento.
 */
public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.metodo();
    }

    private void metodo() {
        Evento evento = new Evento("Fiesta");
        evento.addEventoListener(listener);
        evento.addEventoListener(new Listener1());
        evento.addEventoListener(new Listener2());
        evento.addEventoListener(new Listener3());
        evento.lanzar();
    }

    EventoListener listener = new EventoListener() {
        @Override
        public void evento(Evento evento) {
            System.out.println("Ejecutando " +evento.getNombre()+ " main");
        }
    };
}
