package multiples_listeners;

/**
 * Creada por david minaya el 01/11/2017
 */
public class Listener2 implements EventoListener {

    @Override
    public void evento(Evento evento) {
        System.out.println("Ejecutando " +evento.getNombre()+ " listener 2");
    }
}
