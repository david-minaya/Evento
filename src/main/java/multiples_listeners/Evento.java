package multiples_listeners;

import java.util.ArrayList;

/**
 * Creada por david minaya el 31/10/2017
 */
public class Evento{

    private String nombre;
    ArrayList<EventoListener> listeners = new ArrayList<>();

    public Evento(String nombre) {
        this.nombre = nombre;
    }

    public void addEventoListener(EventoListener listener) {
        listeners.add(listener);
    }

    public void lanzar() {

        for (EventoListener listener : listeners) {
            listener.evento(this);
        }
    }

    public String getNombre() {
        return nombre;
    }

}
