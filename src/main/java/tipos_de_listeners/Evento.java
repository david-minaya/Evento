package tipos_de_listeners;

/**
 * Creada por david minaya el 31/10/2017
 */
public class Evento{

    EventoListener listener;

    public void addEventoListener(EventoListener listener) {
        this.listener = listener;
    }

    public void lanzar() {
        listener.evento(this);
    }

}
