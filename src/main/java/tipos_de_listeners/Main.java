package tipos_de_listeners;

/**
 * Creada por david minaya el 31/10/2017
 *
 * En esta clase se muestran las diferentes maneras de
 * implementar un listener.
 */
public class Main implements EventoListener {

    public static void main(String[] args) {
        Main main = new Main();
        main.metodo();
    }

    private void metodo() {

        // Evento 1
        Evento evento1 = new Evento();
        evento1.addEventoListener(new EventoListener() { // Clase anonima del listener
            @Override
            public void evento(Evento evento) {
                System.out.println("Clase anonima del listener");
            }
        });
        evento1.lanzar();

        // Evento 2
        Evento evento2 = new Evento();
        evento2.addEventoListener(listener2);
        evento2.lanzar();

        // Evento 3
        Evento evento3 = new Evento();
        evento3.addEventoListener(this);
        evento3.lanzar();
    }

    // Objeto del Listener
    EventoListener listener2 = new EventoListener() {
        @Override
        public void evento(Evento evento) {
            System.out.println("Objeto del listener");
        }
    };

    // Implementacion del listener
    @Override
    public void evento(Evento evento) {
        System.out.println("Implementacion del listener");
    }
}
