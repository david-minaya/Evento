package multiples_eventos;

/**
 * Creada por david minaya el 31/10/2017
 *
 * En esta clase se muestra como lanzar varios eventos
 * que se escuchan en un mismo listener.
 */
public class Main {

    public static void main(String[] args) {

        Main main = new Main();
        main.metodo();
    }

    private void metodo() {

        // Evento 1
        Evento evento1 = new Evento("Evento 1");
        evento1.addEventoListener(listener);
        evento1.lanzar();

        // Evento 2
        Evento evento2 = new Evento("Evento 2");
        evento2.addEventoListener(listener);
        evento2.lanzar();
    }

    EventoListener listener = new EventoListener() {
        @Override
        public void evento(Evento evento) {
            System.out.println("Ejecutando " +evento.getNombre());
        }
    };
}
