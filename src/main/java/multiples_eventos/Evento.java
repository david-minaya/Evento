package multiples_eventos;

/**
 * Creada por david minaya el 31/10/2017
 */
public class Evento{

    private String nombre;
    EventoListener listener;

    public Evento(String nombre) {
        this.nombre = nombre;
    }

    public void addEventoListener(EventoListener listener) {
        this.listener = listener;
    }

    public void lanzar() {
        listener.evento(this);
    }

    public String getNombre() {
        return nombre;
    }

}
