# Eventos Java

Un evento es una acción que se dispara cuando se realiza alguna clase de interacción con la aplicación. Un evento puede ejecutarse cuando un usuario presiona una tecla, da un click, mueve el puntero, toca la pantalla (táctil), etc... En aplicación sin interfaz gráfica, un evento se puede ejecutar al modificar el atributo de un objeto, al instanciarlo, llamar un método, entre otras cosas.

Los eventos están compuestos por dos partes: el disparador y el oyente. El disparador es el que lanza el evento y el oyente es el que escucha el evento.

## Interface

Para implementar un evento es necesario crea una interfaz, que es la que poseerá el método (Pueden ser varios) que se llamara para disparar el evento, el cual se implementara en la clase que escuche el evento. El nombre por convención que recibe la interfaz que se utiliza para la creación de evento es "**Listener**". 

>   Listener en español significa "oyente".

Una interfaz se declara de la siguiente manera:

```java
public interface EventoListener {
  
    // Método que se llama para disparar el evento y
    // se implementa para escucharlo.
    public void lanzarEvento(Evento evento);
  
}
```

El método que se declara en la interfaz, es un método sin cuerpo "{}". Este método se llamara desde donde se lanza el evento y se implementa donde se escucha el evento. A este método, al igual que cualquier otro método, se le pueden pasar varios parámetros, pero por convención solo se le debe pasar uno. 

### Lanzar evento

Para lanzar un evento desde una clase, es necesario recibir el objeto de la interfaz que lo escuchara. El objeto se puede recibir en el constructor o desde un método. El nombre del método que recibe el objeto de la interfaz debe iniciar con el prefijo "**add**".

```java
public class Evento {

  EventoListener listener;
    
    public Evento(EventoListener listener) {
      this.listener = listener;
    }
  
    public void addEventListener(EventoListener listener) {
      this.listener = listener;
    }
  
    public lanzar() {...}
}
```

Con el objeto de la interfaz que escuchara el evento recibido, para lanzarlo, solo hay que llamar el método de la interfaz.

```java
public lanzar() {
  listener.evento(this);
}
```

En este caso se le pasa como parámetro **this** "*que es la instancia actual del objeto*".

>   #### this
>
>   this accede al objeto que se esta ejecutando actualmente, es como llamar el objeto desde dentro de el mismo. this es como lo siguiente `new Evento()` pero no de una nueva instancia, si no de la actual.

### Escuchar evento

Para escuchar el evento lanzado desde una clase, es necesario implementar la interfaz del evento y enviar un objeto de la interfaz a la clase que lanza el evento.

```java
public class Main {
  
  public static void main(String[] args) {
    
    Evento evento = new Evento();
    evento.addEventoListener(new EventoListenr {
      @Override
      public void evento(Evento evento) {
        System.out.println("Evento recibido");
      }
    });
    evento.lanzar(); 
  } 
}
```

#### Implementaciones de la interfaz

La interfaz del evento se puede implementar de tres maneras diferentes: creando una clase anónima, implementando la interfaz en la clase o creando un objeto de la interfaz. 

Al implementar una interfaz, todos los métodos de esta se sobrescriben. Los métodos sobrescritos por lo general tienen la anotación `@Override`.

##### 1. Clase anónima

Las clases anónimas son las que se declaran como parámetro (dentro de paréntesis), como en el bloque de código anterior.

##### 2. Implementación de la interfaz en la clase

Para implementar una interfaz en una clase, se utiliza la palabra reservada `implements` seguida del nombre de la interfaz. Esto se escribe después del nombre de la clase y antes de la llave de apertura "{".

```java
public class Main implements EventoListener{
  
  public static void main(String[] args) {
      Main main = new Main();
      main.metodo();
  }
  
  public void metodo() {
      Evento evento = new Evento();
      evento.addEventoListener(this);
      evento.lanzar();
  }
  
  @Override
  public void evento(Evento evento) {
      System.out.println("Evento recibido");
  }
}
```

La palabra reservada `this` no se puede utilizar dentro de un método `static`, como el método `main()`,  si se quiere implementar la interfaz en la clase principal de la aplicación, es necesario crear un objeto de esta y llamar un método no estático que pase el objeto de la interfaz a la clase que lanza el evento. En este caso, el objeto de la interfaz es el objeto de la misma clase. Ya que al implementar la interfaz en esta, esta pasa a ser del mismo tipo que la interfaz.

##### 3. Crear un objeto de la interfaz

```java
public class Main {
  
  public static void main(String[] args) {
      Evento evento = new Evento();
      evento.addEventoListener(listener);
      evento.lanzar(); 
  } 
  
  // Objeto de la intefaz
  EventoListener listener = new EventoListenr {
      @Override
      public void evento(Evento evento) {
          System.out.println("Evento recibido");
      }
  };
}
```

## Multiples eventos

Un oyente puede recibir varios eventos. Para esto, hay que tener en cuenta que todos los eventos deben recibir el mismo objeto de la interfaz del evento.

```java
public class Evento {

  String nombre;
    ...
    
    public Evento(String nombre) {
      this.nombre = nombre;
    }
    ...
      
    public String getNombre() {
        return nombre;  
    }
}
```

```java
public class Main {
  
    public static void main(String[] args) {
        Evento evento1 = new Evento("evento1");
        evento1.addEventoListener(listener);
        evento1.lanzar();
    
        Evento evento2 = new Evento("evento2");
        evento2.addEventoListener(listener);
        evento2.lanzar();
    } 
  
    EventoListener listener = new EventoListenr {
        @Override
        public void evento(Evento evento) {
            System.out.println(evento.getNombre());
        }
    };
}
```

##### Log

```
evento1
evento2
```

## Múltiples oyentes

Un evento también puede tener varios oyentes. Para eso en la clase donde se lanza el evento hay que crear un arrayList que almacene los objetos de la interfaz que escucharan el evento, después hay que recorrer el arrayList y lanzar los eventos a los diferentes objetos de la interfaz.

```java
public class Evento {
  
  String nombre;
  ArrayList<EventoListener> listeners = new ArrayList<>();
  
  public Evento(String nombre) {
    this.nombre = nombre;
  }
  
  public void addEventListener(EventoListener listener) {
    listeners.add(listener);
  }
  
  public void lanzar() {
    for (MyEventListener listener : listeners) {
        listener.evento(this);
    }
  }
  
  public String getNombre() {
    return nombre;
  }
}
```

```java
public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.metodo();
    }

    private void metodo() {
        Evento evento = new Evento("Fiesta");
        evento.addEventoListener(listener);
        evento.addEventoListener(new Listener1());
        evento.addEventoListener(new Listener2());
        evento.lanzar();
    }

    MyEventListener listener = new MyEventListener() {
        @Override
        public void evento(Evento evento) {
            System.out.println("Ejecutando " +evento.getNombre()+ " main");
        }
    };
}
```

Todas las clases que escucharan el evento deben implementar la interfaz del evento.

```java
public class Listener1 implements MyEventListener {

    @Override
    public void evento(Evento evento) {
        System.out.println("Ejecutando " +evento.getNombre()+ " listener 1");
    }
}
```

```java
public class Listener2 implements MyEventListener {

    @Override
    public void evento(Evento evento) {
        System.out.println("Ejecutando " +evento.getNombre()+ " listener 2");
    }
}
```

##### Log

```
Ejecutando Fiesta main
Ejecutando Fiesta listener 1
Ejecutando Fiesta listener 2
Ejecutando Fiesta listener 3
```

### Referencias

-   **[Events (Oracle)](https://docs.oracle.com/javase/tutorial/uiswing/events/intro.html)**
-   **[Eventos propios (Dis)](http://dis.um.es/~bmoros/Tutorial/parte11/cap11-12.html)**
-   **[Creación de eventos personalizados (ricki9)](https://ricki9.wordpress.com/2013/11/21/java-creacion-de-eventos-personalizados-delegados/)**
-   **[Eventos personalizados (YouTube)](https://www.google.com.do/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwj9hq2a66LXAhVCzmMKHeKxDuoQtwIIJTAA&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DB-f7gewzJh8&usg=AOvVaw2HA-H911GxdLDt5evPi9Ed)**
-   **[Creación de eventos en java (panamahitek)](http://panamahitek.com/creacion-de-eventos-en-java/)**